set in_filename "register_description"
set vhd_file "block_regs.vhd"
set entity_name "block_regs"

oo::class create Register {
    
    variable Name
    variable Address
    variable Width
    variable AccessType
    variable DefaultValue

    method getName         {} { return $Name }
    method getAddress      {} { return $Address }
    method getWidth        {} { return $Width }
    method getAccessType   {} { return $AccessType }
    method getDefaultValue {} { return $DefaultValue }
}

set fp [open ${in_filename} r]
set description [read ${fp}]
close ${fp}

set description [split ${description} "\n"]

set avln_addr_width 0
set registers ""
set const_registers ""
set j 0
set last_reg_name ""

foreach i ${description} {
    # puts ${i}
    set tmp [regsub -all { +} ${i} { }]
    set tmp [split ${tmp} " "]
    switch [lindex ${tmp} 0] {
        "#" { 
            continue 
        }
        "" {
            continue
        }
        ":aw" { 
            set avln_addr_width [lindex ${tmp} 2] 
        }
        ":reg" { 
            set tmp_reg [Register new]
            set ${tmp_reg}::Name [lindex ${tmp} 2]
            set ${tmp_reg}::Address [lindex ${tmp} 3]
            set ${tmp_reg}::Width [lindex ${tmp} 4]
            set ${tmp_reg}::AccessType [lindex ${tmp} 5]
            set ${tmp_reg}::DefaultValue [lindex ${tmp} 6]
            set last_reg_name [lindex ${tmp} 2]
            if {[lindex ${tmp} 5] == "RC"} {
                lappend const_registers ${tmp_reg}
            } else {
                lappend registers ${tmp_reg}
            }
        }
        default { 
            puts "Unrecognize command: ${i}"
            exit
        }
    }
}

set fp [open ${vhd_file} w]
puts ${fp} "library ieee; \nuse ieee.std_logic_1164.all; \nuse ieee.std_logic_arith.all; \nuse ieee.std_logic_unsigned.all; \n"
puts ${fp} "entity ${entity_name} is"
puts ${fp} "    generic("
puts ${fp} "        AVLN_ADDR_WIDTH : integer := ${avln_addr_width};"
set ending ";"
foreach i ${const_registers} {
    set name [string toupper [${i} getName]]
    set address [${i} getAddress]
    set default [${i} getDefaultValue]
    if { [${i} getName] == ${last_reg_name} } {
        set ending ""
    }
    puts ${fp} "        AD_${name} : integer := ${address};"
    puts ${fp} "        DEFAULT_${name} : integer := ${default}${ending}"
}
foreach i ${registers} {
    set name [string toupper [${i} getName]]
    set address [${i} getAddress]
    set default [${i} getDefaultValue]
    if { [${i} getName] == ${last_reg_name} } {
        set ending ""
    }
    if {${default} == ""} {
        puts ${fp} "        AD_${name} : integer := ${address}${ending}"
    } else {
        puts ${fp} "        AD_${name} : integer := ${address};"
        puts ${fp} "        DEFAULT_${name} : integer := ${default}${ending}"
    }
}
puts ${fp} "    );"
puts ${fp} "    port("
puts ${fp} "        clk : in std_logic;"
puts ${fp} "        reset : in std_logic;"
puts ${fp} "        CP_address : in std_logic_vector([expr ${avln_addr_width} - 1] downto 0);"
puts ${fp} "        CP_write : in std_logic;"
puts ${fp} "        CP_writedata : in std_logic_vector(31 downto 0);"
puts ${fp} "        CP_read : in std_logic;"
puts ${fp} "        CP_readdata : out std_logic_vector(31 downto 0);"
puts ${fp} "        CP_waitrequest : out std_logic;"
set ending ";"
foreach i ${registers} {
    set name [${i} getName]
    set up_idx [expr [${i} getWidth] - 1]
    set dir [${i} getAccessType]
    if { [${i} getName] == ${last_reg_name} } {
        set ending ""
    }
    switch ${dir} {
        "RO" {
            puts ${fp} "        ${name} : in std_logic_vector(${up_idx} downto 0)${ending}"
        }
        "WO" {
            puts ${fp} "        ${name} : out std_logic_vector(${up_idx} downto 0)${ending}"
        }
        "RW" {
            puts ${fp} "        ${name}_in : in std_logic_vector(${up_idx} downto 0)${ending}"
            puts ${fp} "        ${name}_out : out std_logic_vector(${up_idx} downto 0)${ending}"
            puts ${fp} "        ${name}_out_vld : out std_logic;"
        }
    }
}
puts ${fp} "    );"
puts ${fp} "end ${entity_name};"
puts ${fp} "\n"
puts ${fp} "architecture rtl of ${entity_name} is"
puts ${fp} "    signal addr_reg : std_logic_vector([expr ${avln_addr_width} - 1] downto 0);"
puts ${fp} "    signal wait_flag : std_logic;"
foreach i ${registers} {
    set name [${i} getName]
    set up_idx [expr [${i} getWidth] - 1]
    puts ${fp} "    signal ${name}_int : std_logic_vector(${up_idx} downto 0);"
}
puts ${fp} "begin"
foreach i ${registers} {
    set name [${i} getName]
    switch [${i} getAccessType] {
        "RO" {
            continue
        }
        "WO" {
            puts ${fp} "    ${name} <= ${name}_int;"
        }
        "RW" {
            puts ${fp} "    ${name}_out <= ${name}_int;"
        }
    }
}
puts ${fp} "\n"
set j 0
set prefix "CP_readdata <="
foreach i ${registers} {
    if { ${j} > 0} {
        set prefix "else"
    }
    set name [${i} getName]
    set addr [string toupper ${name}]
    if { [${i} getAccessType] == "RO" } {
        puts ${fp} "    ${prefix} ${name} when addr_reg = AD_${addr}"
    } elseif { [${i} getAccessType] == "RW" } {
        puts ${fp} "    ${prefix} ${name}_in when addr_reg = AD_${addr}"
    }
    incr j
}
foreach i ${const_registers} {
    if { ${j} > 0 } {
        set prefix "else"
    }
    set name [${i} getName]
    set default [${i} getDefaultValue]
    set addr [string toupper ${name}]
    puts ${fp} "    ${prefix} conv_std_logic_vector(${default}, CP_readdata'length) when addr_reg = AD_${addr} -- reading of ${name}"
}
if {${j} > 0} {
    puts ${fp} "    else (others => '0');"
}
puts ${fp} "\n"
puts ${fp} "    CP_waitrequest <= not wait_flag;"
puts ${fp} "    process(clk, reset)"
puts ${fp} "    begin"
puts ${fp} "        if reset = '1' then"
puts ${fp} "            addr_reg <= (others => '0');"
puts ${fp} "            wait_flag <= '0';"
foreach i ${registers} {
    set name [${i} getName]
    set default [${i} getDefaultValue]
    if {${default} == ""} {
        set default_assign "(others => '0');"
    } else {
        set default_assign "conv_std_logic_vector(${default}, ${name}_int'length);"
    }
    switch [${i} getAccessType] {
        "RO" {
            continue
        }
        "WO" {
            puts ${fp} "            ${name}_int <= ${default_assign}"
        }
        "RW" {
            puts ${fp} "            ${name}_int <= ${default_assign}"
            puts ${fp} "            ${name}_out_vld <= '0';"
        }
    }
}
puts ${fp} "        else"
puts ${fp} "            if rising_edge(clk) then"
puts ${fp} "                if (CP_read = '1' or CP_write = '1') and wait_flag = '0' then"
puts ${fp} "                    wait_flag <= '1';"
puts ${fp} "                    addr_reg <= CP_address;"
puts ${fp} "                else"
puts ${fp} "                    wait_flag <= '0';"
puts ${fp} "                end if;"
foreach i ${registers} {
    set name [${i} getName]
    set addr [string toupper ${name}]
    if { [${i} getAccessType] == "WO"} {
        puts ${fp} "                if CP_write = '1' and wait_flag = '1' and addr_reg = AD_${addr} then"
        puts ${fp} "                    ${name}_int <= ext(CP_writedata, ${name}_int'length);"
        puts ${fp} "                end if;"
    } elseif { [${i} getAccessType] == "RW"} {
        puts ${fp} "                if CP_write = '1' and wait_flag = '1' and addr_reg = AD_${addr} then"
        puts ${fp} "                    ${name}_int <= ext(CP_writedata, ${name}_int'length);"
        puts ${fp} "                    ${name}_out_vld <= '1';"
        puts ${fp} "                else"
        puts ${fp} "                    ${name}_out_vld <= '0';"
        puts ${fp} "                end if;"
    }
}

puts ${fp} "            end if;"
puts ${fp} "        end if;"
puts ${fp} "    end process;"

puts ${fp} "end rtl;"

close ${fp}